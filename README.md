# wk-challenge

Teste para WKRH baseado em Django + Docker.

Leia os requisitos do projeto em [desafio.md](desafio.md).


## Este projeto foi feito com:

* [Python 3.8.2](https://www.python.org/)
* [Django 3.1.6](https://www.djangoproject.com/)
* [Bootstrap 4.0](https://getbootstrap.com/)

## Como rodar o projeto?

```
git clone https://gitlab.com/rg3915/wk-challenge.git
cd wk-challenge
python contrib/env_gen.py
docker-compose up --build -d
```

O comando `python contrib/env_gen.py` cria um arquivo `.env` onde você pode definir as variáveis de ambiente:

```
POSTGRES_DB=mydb
POSTGRES_USER=myuser
POSTGRES_PASSWORD=mypass

# or smtp.EmailBackend
EMAIL_BACKEND=django.core.mail.backends.console.EmailBackend
EMAIL_HOST=smtp.gmail.com
EMAIL_PORT=587
EMAIL_HOST_USER=
EMAIL_HOST_PASSWORD=
EMAIL_USE_TLS=True
```

A aplicação estará rodando na porta 8000.

O painel de admin está em 8000/admin.


Entre no container e crie um superuser:

```
docker container exec -it \
wk-challenge_app_1 \
python manage.py createsuperuser \
--username="admin" --email=""
```

Criando alguns dados

```
docker container exec -it \
wk-challenge_app_1 \
python manage.py create_data
```

Conectando diretamente no banco.

```
docker container exec -it wk-challenge_db_1 psql -h db -U myuser mydb
# mypass
```

Monitorando o celery via terminal

```
docker container logs -f wk-challenge_celery_1
```

Monitorando o celery via flower em http://0.0.0.0:5555/



**Avisos:**

Se quiser enviar e-mail de verdade, troque

```
EMAIL_BACKEND=django.core.mail.backends.console.EmailBackend
# por
EMAIL_BACKEND=django.core.mail.backends.smtp.EmailBackend
```

O `DB_HOST=db` está configurado pra rodar no Docker, se quiser rodar na máquina local deve-se mudar para `DB_HOST=localhost`.





## Arquitetura

![diagrama](diagrama2.png)


## Tabelas

Usei [pygraphviz](https://gist.github.com/rg3915/35e999a442a8955e455b) para gerar o modelo com o comando:

```
python manage.py graph_models -e -g -l dot -o crm.png crm
```

![crm](crm.png)
