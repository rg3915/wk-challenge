from datetime import date
from dateutil.relativedelta import *
from decouple import config
from django.core.mail import EmailMultiAlternatives
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags


def send_email_simple(subject: str, body: str, sender: str, receiver: list):
    send_mail(subject, body, sender, receiver)


def send_email_html(subject: str, body: str, sender: str, receiver: list):
    msg = EmailMultiAlternatives(subject, strip_tags(body), sender, receiver)
    msg.attach_alternative(body, "text/html")
    msg.send()


def expire_in(queryset, period: int, month_or_week: str):
    if month_or_week == 'm':
        period_delta = relativedelta(months=+period)
    elif month_or_week == 'w':
        period_delta = relativedelta(weeks=+period)
    expiration_date = date.today() + period_delta
    return queryset.filter(expiration_date=expiration_date)


def send_one_email(customer, seller, licenses, period, month_or_week):
    subject = f'As licenças de {customer.name} vão expirar em {period} {month_or_week}'
    context = {
        'seller_name': seller.get_full_name(),
        'customer_name': customer.name,
        'customer_email': customer.email,
        'quantity_licenses': licenses.count(),
        'licenses': licenses,
    }
    html_content = render_to_string('emails/email_licenses.html', context)
    sender = config('EMAIL_HOST_USER')
    receiver = [config('EMAIL_HOST_USER'), ]
    send_email_html(subject=subject, body=html_content, sender=sender, receiver=receiver)
