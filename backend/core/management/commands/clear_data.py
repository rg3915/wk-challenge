from backend.crm.models import Seller, Customer, TypeLicense, License
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Deleta os dados do banco.'

    def handle(self, *args, **options):
        User.objects.exclude(username='admin').delete()
        Seller.objects.all().delete()
        Customer.objects.all().delete()
        License.objects.all().delete()
        self.stdout.write('Dados deletados com sucesso.')
