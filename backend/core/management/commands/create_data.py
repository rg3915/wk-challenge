import names
import string
from random import randint
from datetime import date, datetime, timedelta
from dateutil.relativedelta import *
from random import choice, random
from backend.crm.models import Seller, Customer, TypeLicense, License
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.utils.text import slugify


def gen_string(max_length):
    return str(''.join(choice(string.ascii_letters) for i in range(max_length)))


gen_string.required = ['max_length']


def gen_digits(max_length: int):
    '''Gera dígitos numéricos.'''
    return str(''.join(choice(string.digits) for i in range(max_length)))


def gen_date(min_year=2021, max_year=datetime.now().year):
    # gera um date no formato yyyy-mm-dd
    start = date(min_year, 1, 1)
    years = max_year - min_year + 1
    end = start + timedelta(days=365 * years)
    return start + (end - start) * random()


def gen_expiration_date(period: int, month_or_week: str):
    if month_or_week == 'm':
        return date.today() + relativedelta(months=+period)
    if month_or_week == 'w':
        return date.today() + relativedelta(weeks=+period)


def gen_first_name():
    return names.get_first_name()


def gen_last_name():
    return names.get_last_name()


def gen_email(first_name: str, last_name: str, company: str = None):
    EMAIL = ('email', 'gmail', 'yahoo', 'hotmail', 'uol')
    first_name = slugify(first_name)
    last_name = slugify(last_name)
    sufix = choice(EMAIL)
    email = '{}.{}@{}.com'.format(first_name, last_name, sufix)
    return email


def update_password_all_users():
    users = User.objects.all()
    for user in users:
        user.set_password('d')
        user.save()


def create_seller():
    n = 3
    for _ in range(n):
        first_name = gen_first_name()
        last_name = gen_last_name()
        Seller.objects.get_or_create(
            username=last_name.lower(),
            first_name=first_name,
            last_name=last_name,
            email=gen_email(first_name, last_name),
        )


def create_customer():
    n = 15
    aux = []
    for _ in range(n):
        first_name = gen_first_name()
        last_name = gen_last_name()
        full_name = f'{first_name} {last_name}'
        obj = Customer(
            name=full_name,
            email=gen_email(first_name, last_name),
        )
        aux.append(obj)
    Customer.objects.bulk_create(aux)


def create_type_license():
    items = ('Basic', 'Standard', 'Premium')
    for item in items:
        TypeLicense.objects.get_or_create(name=item)


def create_license():
    # Gera xx (random) licenças para cada cliente.
    # Com datas de vencimento variando entre 1 semana, 1 mês ou 4 meses.
    n = randint(20, 50)
    customers = Customer.objects.all()
    type_licenses = TypeLicense.objects.all()
    sellers = Seller.objects.all()

    aux = []
    for customer in customers:
        for i in range(n):
            if i % 7 == 0:
                expiration_date = gen_expiration_date(4, 'm')
            elif i % 5 == 0:
                expiration_date = gen_expiration_date(1, 'm')
            else:
                expiration_date = gen_expiration_date(1, 'w')
            type_license = choice(type_licenses)
            seller = choice(sellers)
            obj = License(
                name=f'{gen_string(3).upper()}{gen_digits(3)}',
                type_license=type_license,
                expiration_date=expiration_date,
                customer=customer,
                seller=seller,
            )
            aux.append(obj)
    License.objects.bulk_create(aux)


class Command(BaseCommand):
    help = 'Gera alguns dados aleatórios.'

    def handle(self, *args, **options):
        create_seller()
        # update_password_all_users()
        create_customer()
        create_type_license()
        create_license()
