from __future__ import absolute_import, unicode_literals
from celery import shared_task
from decouple import config
from django.template.loader import render_to_string
from backend.crm.models import Customer, License
from .core_send_email import send_email_html, expire_in, send_one_email


@shared_task
def add(x, y):
    return x + y


@shared_task
def send_email_with_licenses_teste():
    # Teste
    customer = Customer.objects.first()
    licenses = License.objects.filter(customer=customer)
    seller = licenses[0].seller

    subject = f'As licenças de {customer.name} vão expirar em breve'
    context = {
        'seller_name': seller.get_full_name(),
        'customer_name': customer.name,
        'customer_email': customer.email,
        'quantity_licenses': licenses.count(),
        'licenses': licenses,
    }
    html_content = render_to_string('emails/email_licenses.html', context)
    sender = config('EMAIL_HOST_USER')
    receiver = [config('EMAIL_HOST_USER'), ]
    send_email_html(subject=subject, body=html_content, sender=sender, receiver=receiver)


@shared_task
def send_email_for_all_sellers_expire_in_4_months():
    # Enviar email para todos vendedores das licenças que expiram em 4 meses.
    customers = Customer.objects.all()
    for customer in customers:
        licenses = License.objects.filter(customer=customer)
        seller = licenses[0].seller
        licenses_expire = expire_in(licenses, 4, 'm')
        send_one_email(customer, seller, licenses_expire, 4, 'meses')


@shared_task
def send_email_for_all_sellers_expire_in_1_months():
    # Enviar email para todos vendedores das licenças que expiram em 1 mês.
    customers = Customer.objects.all()
    for customer in customers:
        licenses = License.objects.filter(customer=customer)
        seller = licenses[0].seller
        licenses_expire = expire_in(licenses, 1, 'm')
        send_one_email(customer, seller, licenses_expire, 1, 'mês')


@shared_task
def send_email_for_all_sellers_expire_in_1_weeks():
    # Enviar email para todos vendedores das licenças que expiram em 1 semana.
    customers = Customer.objects.all()
    for customer in customers:
        licenses = License.objects.filter(customer=customer)
        seller = licenses[0].seller
        licenses_expire = expire_in(licenses, 1, 'w')
        send_one_email(customer, seller, licenses_expire, 1, 'semana')
