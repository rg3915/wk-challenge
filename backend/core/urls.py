from django.urls import path
from backend.core import views as v


app_name = 'core'


urlpatterns = [
    path('', v.index, name='index'),
    path('send-email/', v.send_email_simple_view, name='send_email_simple_view'),
    path('send-email/html/', v.send_email_html_view, name='send_email_html_view'),
    path(
        'send-email/html-template/',
        v.send_email_html_template_view,
        name='send_email_html_template_view'
    ),
    path(
        'send-email/licenses/',
        v.send_email_with_licenses,
        name='send_email_with_licenses'
    ),
]
