from datetime import datetime
from decouple import config

from backend.crm.models import Customer, License
from django.http import HttpResponse
from django.shortcuts import render
from django.template.loader import render_to_string

from .core_send_email import send_email_simple, send_email_html


def index(request):
    template_name = 'index.html'
    total_customers = Customer.objects.all().count()
    total_licenses = License.objects.all().count()
    context = {
        'total_customers': total_customers,
        'total_licenses': total_licenses,
    }
    return render(request, template_name, context)


def send_email_simple_view(request):
    data = datetime.now().isoformat(' ')
    subject = f'Test {data}'
    body = f'Hello World {data}'
    sender = config('SENDER')
    receiver = [config('SENDER'), ]
    send_email_simple(subject, body, sender, receiver)
    return HttpResponse('E-mail enviado com sucesso.')


def send_email_html_view(request):
    data = datetime.now().isoformat(' ')
    subject = f'HTML e-mail {data}'
    html_content = f'<h1>Olá, tudo bem?</h1><p>Mensagem <strong>importante</strong> <i>{data}</i></p>'
    sender = 'admpgsa.regis@gmail.com'
    receiver = ['admpgsa.regis@gmail.com']
    send_email_html(subject=subject, body=html_content, sender=sender, receiver=receiver)
    return HttpResponse('E-mail enviado com sucesso.')


def send_email_html_template_view(request):
    data = datetime.now().isoformat(' ')
    subject = f'HTML e-mail com template {data}'
    context = {'name': 'Regis'}
    html_content = render_to_string('emails/email_hello.html', context)
    sender = 'admpgsa.regis@gmail.com'
    receiver = ['admpgsa.regis@gmail.com']
    send_email_html(subject=subject, body=html_content, sender=sender, receiver=receiver)
    return HttpResponse('E-mail enviado com sucesso.')


def send_email_with_licenses(request):
    customer = Customer.objects.first()
    licenses = License.objects.filter(customer=customer)
    seller = licenses[0].seller

    subject = f'As licenças de {customer.name} vão expirar em breve'
    context = {
        'seller_name': seller.get_full_name(),
        'customer_name': customer.name,
        'customer_email': customer.email,
        'quantity_licenses': licenses.count(),
        'licenses': licenses,
    }
    html_content = render_to_string('emails/email_licenses.html', context)
    sender = 'admpgsa.regis@gmail.com'
    # receiver = [seller.email, 'admpgsa.regis@gmail.com']
    receiver = ['admpgsa.regis@gmail.com']
    send_email_html(subject=subject, body=html_content, sender=sender, receiver=receiver)
    return HttpResponse('E-mail enviado com sucesso.')
