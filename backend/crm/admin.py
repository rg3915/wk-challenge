from django.contrib import admin
from .models import Seller, Customer, TypeLicense, License


@admin.register(Seller)
class SellerAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'email')
    search_fields = ('username', 'first_name', 'last_name', 'email')


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'email')
    search_fields = ('name',)


@admin.register(TypeLicense)
class TypeLicenseAdmin(admin.ModelAdmin):
    list_display = ('__str__',)
    search_fields = ('name',)


@admin.register(License)
class LicenseAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'customer', 'seller', 'expiration_date')
    search_fields = ('name', 'customer__name', 'seller__first_name', 'seller__last_name')
    list_filter = ('type_license', 'seller')
    date_hierarchy = 'expiration_date'
    ordering = ('expiration_date',)
