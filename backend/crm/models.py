from django.db import models
from django.contrib.auth.models import User


class Customer(models.Model):
    name = models.CharField('nome', max_length=100, unique=True)
    email = models.EmailField()

    class Meta:
        ordering = ('name',)
        verbose_name = 'cliente'
        verbose_name_plural = 'clientes'

    def __str__(self):
        return self.name


class Seller(User):

    class Meta:
        ordering = ('first_name',)
        verbose_name = 'vendedor'
        verbose_name_plural = 'vendedores'

    def __str__(self):
        return self.get_full_name()


class TypeLicense(models.Model):
    name = models.CharField('nome', max_length=50)

    class Meta:
        ordering = ('name',)
        verbose_name = 'tipo'
        verbose_name_plural = 'tipos'

    def __str__(self):
        return self.name


class License(models.Model):
    name = models.CharField('nome', max_length=100)
    type_license = models.ForeignKey(
        TypeLicense,
        verbose_name='tipo',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )
    expiration_date = models.DateTimeField('data de validade')
    customer = models.ForeignKey(
        Customer,
        verbose_name='cliente',
        related_name='customers',
        on_delete=models.CASCADE,
    )
    seller = models.ForeignKey(
        Seller,
        verbose_name='vendedor',
        related_name='sellers',
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ('name',)
        verbose_name = 'licença'
        verbose_name_plural = 'licenças'

    def __str__(self):
        return self.name
