# wk-challenge

Teste para WKRH baseado em Django + Docker.

## Desafio

O desafio consiste em uma parte de um aplicativo de licenciamento SDK usado para
permitir que clientes façam download do software proprietário da empresa. O time
de vendas precisa de um recurso que os notifique automaticamente quando uma
das licenças de seus clientes expirar.


## Requisitos

- Python v3.x
- Django
- O projeto deve ser iniciado com um único comando docker-compose up
- O projeto em execução pode ser acessado no navegador em [docker
host]:8080
- O projeto deve ter o módulo administrador acessível em [docker
host]:8080/admin


## Passos

- Implemente um mecanismo de envio de e-mail para notificar o time de
vendas sobre o tempo de expiração das licenças de seus clientes. A
mensagem deve ser enviada caso atenda os seguintes pontos:
    - O cliente tem licenças que expiram em exatamente 4 meses;
    - O cliente tem licenças que expiram em 1 mês e hoje é segunda-feira;
    - O cliente tem licenças que expiram em 1 semana;
- Todos os critérios anteriores.
- O corpo do e-mail deve conter uma lista de todas as licenças do cliente e os
e-mails devem incluir apenas detalhes do cliente em questão, ou seja, um
e-mail por cliente.
- As notificações por e-mail devem incluir:
    - Id da licença;
    - Tipo da licença;
    - Nome do pacote;
    - Data de validade;
    - Nome e endereço de e-mail do cliente.
- Crie um front-end para utilizar os endpoints.
- A aplicação deve estar separada em dois projetos um para a API e outro para
o front-end.

**Bônus**: Crie a documentação da API com algum serviço externo, exemplo:
Swagger.

O que vamos avaliar
- Organização do código
- Documentação do código
- Mensagens e mudanças nos commits